package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/francoispqt/onelog"

	"net/http"
	_ "net/http/pprof"

	"github.com/kelseyhightower/envconfig"
)

func main() {

	errs := make(chan error, 10)
	shutdown := make(chan struct{}, 1)
	ctx, cancel := context.WithCancel(context.Background())

	config := &config{}
	if err := envconfig.Process("cbr", config); err != nil {
		errs <- err
	}

	onelog.MsgKey("msg")

	level := onelog.INFO | onelog.WARN | onelog.ERROR | onelog.FATAL

	logger := onelog.
		New(os.Stderr, level).
		With(func(e onelog.Entry) {
			// time
			e.String("ts", time.Now().Format(time.RFC3339Nano))
			// caller
			_, file, line, _ := runtime.Caller(3)
			e.String("caller", strings.Join([]string{file, strconv.Itoa(line)}, ":"))
		})

	router, err := setup(
		config,
		logger,
	)
	if err != nil {
		errs <- err
	}

	go func() {
		logger.InfoWithFields("listening", func(entry onelog.Entry) {
			entry.String("address", ":6060")
		})
		if err := http.ListenAndServe(":6060", nil); err != nil && err != http.ErrServerClosed {
			errs <- err
		}
	}()

	server := &http.Server{
		ReadTimeout:  config.Timeout,
		WriteTimeout: config.Timeout,
		Addr:         ":8080",
		Handler:      router,
	}

	go func(server *http.Server) {
		logger.InfoWithFields("listening", func(entry onelog.Entry) {
			entry.String("address", server.Addr)
		})
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			errs <- err
		}
	}(server)

	go func() {
		select {
		case err = <-errs:
		case <-ctx.Done():
			err = ctx.Err()
		}
		if err != nil {
			logger.Error(err.Error())
		}

		cancel()
		server.SetKeepAlivesEnabled(false)

		logger.Info("3 second timeout is started")

		time.Sleep(3 * time.Second)
		shutdown <- struct{}{}
	}()

	go func() {
		signals := make(chan os.Signal, 1)
		signal.Notify(
			signals,
			syscall.SIGINT,
			syscall.SIGTERM,
		)
		errs <- fmt.Errorf("%s", <-signals)
	}()

	<-shutdown

	if err := server.Shutdown(context.Background()); err != nil {
		logger.Fatal(fmt.Sprintf("Server Shutdown: %s", err.Error()))
	}

	logger.Info("service shutdown")
}

type config struct {
	Timeout  time.Duration `envconfig:"timeout" default:"30s"`
	Radius   float64       `envconfig:"radius" default:"5"`
	SOAPLink string        `envconfig:"soap_link"`
	Currency string        `envconfig:"currency"`
}
