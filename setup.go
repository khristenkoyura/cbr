package main

import (
	"cbr/cbr"
	"net/http"
	"time"

	"github.com/francoispqt/onelog"

	"github.com/tiaguinho/gosoap"

	"github.com/gin-gonic/gin/binding"

	"github.com/gin-gonic/gin"
)

func setup(
	config *config,
	logger *onelog.Logger,
) (
	router *gin.Engine,
	err error,
) {
	gin.SetMode(gin.ReleaseMode)

	// Меняем валидатор с 8 версий на 10 версию
	binding.Validator = new(defaultValidator)

	router = gin.New()
	router.Use(Logger(logger))
	router.Use(gin.Recovery())

	httpClient := &http.Client{
		Timeout: 30 * time.Second,
	}
	soap, err := gosoap.SoapClient(config.SOAPLink, httpClient)
	if err != nil {
		return
	}

	s := cbr.Service{
		CurrencyCode: config.Currency,
		Client:       soap,
	}
	endpoint := cbr.Endpoints{
		Service: &s,
		Radius:  config.Radius,
	}

	endpoint.AddHandlers(router)
	return
}
