package cbr

import (
	"cbr/cbr/wsdl"
	"fmt"
	"math"
	"time"

	"github.com/tiaguinho/gosoap"

	"github.com/gin-gonic/gin"
)

// Point точка координат
type Point struct {
	X float64 `validate:"required"`
	Y float64 `validate:"required"`
}

// Form форма принятия запроса
type Form struct {
	Point
	Radius float64
}

// Validate валидация входных параметров
func (f *Form) Validate(c *gin.Context) (err error) {
	if err = c.ShouldBindJSON(&f.Point); err != nil {
		return err
	}

	distance := math.Sqrt(math.Pow(f.Point.X, 2) + math.Pow(f.Point.Y, 2))
	if distance > f.Radius {
		return fmt.Errorf("координата x:%f y:%f не попала в окружность радиуса R %f", f.Point.X, f.Point.Y, f.Radius)
	}

	return
}

// Day определенния дня по точке координат
func (f *Form) Day() (time.Time, error) {
	x, y := f.Point.X, f.Point.Y
	t := time.Now()
	switch {
	case x > 0 && y > 0:
		return t, nil
	case x > 0 && y < 0:
		return t.AddDate(0, 0, 1), nil
	case x < 0 && y < 0:
		return t.AddDate(0, 0, -2), nil
	case x < 0 && y > 0:
		return t.AddDate(0, 0, -1), nil
	}
	return t, fmt.Errorf("координата x:%f y:%f не должна содержать 0 значение", x, y)
}

// Service получения данных из сервиса центрабанка
type Service struct {
	CurrencyCode string
	Client       *gosoap.Client
}

// Rate получения курса валюты относительно 1 рубля
func (s *Service) Rate(t time.Time) (value float64, err error) {
	params := gosoap.Params{
		"On_date": t.Format(time.RFC3339),
	}

	res, err := s.Client.Call("GetCursOnDateXML", params)
	if err != nil {
		return
	}

	var r wsdl.GetCursOnDateXMLResponse
	err = res.Unmarshal(&r)
	if err != nil {
		return
	}

	for _, currency := range r.Currencies() {
		if currency.VchCode != s.CurrencyCode {
			continue
		}

		value = 1 / currency.Vcurs
		return
	}

	err = fmt.Errorf("нет данных по валюте")
	return
}
