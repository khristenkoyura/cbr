package cbr

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Endpoints обработчики запросов
type Endpoints struct {
	Service *Service
	Radius  float64
}

// AddHandlers привязка обработчиков к Router
func (e *Endpoints) AddHandlers(router gin.IRoutes) {
	router.POST("cbr/rate", e.Rate)
}

func (e *Endpoints) validateError(c *gin.Context, err error) {
	ginError := c.Error(err)
	c.AbortWithStatusJSON(http.StatusUnprocessableEntity, ginError.JSON())
}

func (e *Endpoints) responseError(c *gin.Context, err error) {
	ginError := c.Error(err)
	c.AbortWithStatusJSON(http.StatusInternalServerError, ginError.JSON())
}

// Rate получения курса валюты относительно 1 рубля
func (e *Endpoints) Rate(c *gin.Context) {
	var err error
	form := Form{
		Radius: e.Radius,
	}

	if err = form.Validate(c); err != nil {
		e.validateError(c, err)
		return
	}

	date, err := form.Day()
	if err != nil {
		e.validateError(c, err)
		return
	}

	value, err := e.Service.Rate(date)
	if err != nil {
		e.responseError(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"Value": value})
}
