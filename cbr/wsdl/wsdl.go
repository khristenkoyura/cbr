// Структуры даных для анмаршилинга запросов в soap сервис центрабанка

package wsdl

type GetCursOnDateXMLResponse struct {
	GetCursOnDateXMLResult GetCursOnDateXMLResult
}

func (r *GetCursOnDateXMLResponse) Currencies() []ValuteCursOnDate {
	return r.GetCursOnDateXMLResult.ValuteData.ValuteCursOnDate
}

type GetCursOnDateXMLResult struct {
	ValuteData ValuteData
}

type ValuteData struct {
	ValuteCursOnDate []ValuteCursOnDate
}

type ValuteCursOnDate struct {
	Vname   string
	VchCode string
	Vcurs   float64
}
