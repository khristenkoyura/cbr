module cbr

go 1.13

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20191128021309-1d7a30a10f73
	github.com/francoispqt/gojay v1.2.13
	github.com/francoispqt/onelog v0.0.0-20190306043706-8c2bb31b10a4
	github.com/gin-contrib/gzip v0.0.2
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/go-redis/redis/v7 v7.3.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/juliangruber/go-intersect v1.0.1-0.20200717103803-2e99d8c0a75f
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mcuadros/go-gin-prometheus v0.1.0
	github.com/nats-io/nats.go v1.9.1
	github.com/nats-io/stan.go v0.6.0
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/tiaguinho/gosoap v1.4.4
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
	gopkg.in/guregu/null.v3 v3.4.0
)
